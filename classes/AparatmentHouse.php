<?php


namespace classes;


class AparatmentHouse extends Building
{
    private $apartament = [];

    public function isCountApartment()
    {
        return count($this->apartament);
    }

    public function __set($name, $value)
    {
        parent::__set($name, $value);
    }

    public function __get($name)
    {
        return parent::__get($name);
    }

}