<?php

namespace classes;


class  Building implements OpenableDoor,OpenableWindow
{
    protected $windows;
    protected $doors;
    protected $roof;
    protected $floor;
    protected $room;

    
    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function __get($name)
    {
        return $this->$name;
    }


    public function openDoor()
    {
        // TODO: Implement openDoor() method.
    }

    public function closeDoor()
    {
        // TODO: Implement closeDoor() method.
    }

    public function openWindow()
    {
        // TODO: Implement openWindow() method.
    }

    public function closeWindow()
    {
        // TODO: Implement closeWindow() method.
    }
}