<?php


namespace classes;


class Dugout implements Openable
{
    private $windows;
    private $door;
    private $room;

    public function openDoor()
    {
        echo "Open door";
    }

    public function closeDoor()
    {
        echo "Close door";
    }
    
}