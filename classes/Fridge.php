<?php


namespace classes;


class Fridge extends Wardrobe
{
    private $temperature;
    private $freezerTemperature;

    public function __get($name)
    {
        return parent::__get($name);
    }
    public function __set($name, $value)
    {
        parent::__set($name, $value);
    }
}