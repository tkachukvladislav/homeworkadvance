<?php


namespace classes;


interface LightManagment
{
    public function onLights();
    public function offLights();
}