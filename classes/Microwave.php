<?php


namespace classes;


class Microwave extends Wardrobe
{
    private $timer;
    private $temperature;

    public function __get($name)
    {
        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        parent::__set($name, $value);
    }
}