<?php


namespace classes;


interface OpenableDoor
{
    public function openDoor();

    public function closeDoor();
}