<?php


namespace classes;


interface OpenableWindow
{
    public function openWindow();

    public function closeWindow();
}