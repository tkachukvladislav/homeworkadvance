<?php


namespace classes;


class PrivateHouse extends Building
{
    private $garage;

    public function openGarage()
    {
        echo "Open Garage";
    }

    public function closeGarage()
    {
        echo "Close Garage";
    }

    public function __get($name)
    {
        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        parent::__set($name, $value);
    }
}