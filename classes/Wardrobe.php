<?php


namespace classes;


class Wardrobe implements OpenableDoor, LightManagment
{
    protected $light;
    protected $door;

    public function __set($name, $value)
    {
        $this->$name = $value;
    }
    public function __get($name)
    {
        return $this->$name;
    }

    public function openDoor()
    {
        echo "open Door";
    }

    public function closeDoor()
    {
        echo "close Door";
    }

    public function onLights()
    {
        echo "On Lights";
    }

    public function offLights()
    {
        echo "Off Lights";
    }
}