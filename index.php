<?php

use classes\Building;
use classes\PrivateHouse;
use classes\Microwave;

require_once __DIR__ . '/vendor/autoload.php';

$build = new Building();
$build->floor = 5;
$build->windows = 3;
$build->doors = 1;
$build->roof = "Metal";

$privateHouse = new PrivateHouse();
$microwave = new Microwave();

